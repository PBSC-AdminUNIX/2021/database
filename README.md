# VPN

## Integrantes
* Sanchez Arevalo Rodrigo Angel.
* Patiño Maza Ismael Zinedine.
* Diaz de Luis Billy.

![Alt text](./img/vpn.png?raw=true "Background")

##  Modulo VPN 
 
El modulo asignado a nuestro equipo es el modulo VPN por
sus siglas Virtual Private Network o red privada virtual,una conexión VPN  permite crear una red local sin necesidad
que sus integrantes estén físicamente conectados entre sí,sino a través de Internet.obteniendo las ventajas de la red
local , con una mayor flexibilidad, pues la conexión es a través de Internet.

Para la implementacion del modulo emplearemos OpenVPN,OpenVPN es una solución VPN de seguridad en la capa de transporte (TLS)
de código abierto y con características completas que aloja muchas configuraciones. Configuraremos OpenVPN en un servidor 
CentOS 8, finalmente se configurará para que sea accesible desde la máquina de un cliente.

[Manual de configuracion para conectarse con openvpn](manual_conexion_openvpn.md)

## 1. Instalacion de  openVPN 
 
Como primer paso debemos instalar OpenVPN y Easy-RSA. Easy-RSA es una herramienta de gestión de infraestructura de clave
pública (PKI) que usará en el servidor de OpenVPN para generar una solicitud de certificado, que posteriormente verificará y firmará
en el servidor CA.

OpenVPN y Easy-RSA no vienen predeterminadas en CentOS 8, por lo tanto habilitamos el repositorio Extra Packages for 
Enterprise Linux (EPEL).

Iniciamos sesión en nuestro servidor de OpenVPN con un usuario con privilegios sudo  y  ejecutamos.
 
 Codigo:
 
    sudo dnf install epel-release 
	sudo dnf install openvpn easy-rsa 
 
Se debe crear un directorio nuevo en el servidor donde estará el servicio de OpenVPN:
 
 Codigo: 
 
    mkdir ~/easy-rsa 
 
Creamos un enlace simbolico desde la secuencia de comandos de easyrsa que el paquete instaló en el directorio `~/easy-rsa` que se acaba de crear.
 
 Codigo: 
 
	ln -s /usr/share/easy-rsa/3/* ~/easy-rsa/
 
Para finalizar, nos aseguramos de que el propietario del directorio sea nuestro usuario y restringimos el acceso a solamente ese usuario. 
 
 Codigo: 
 
	sudo chown becarios ~/easy-rsa 
	chmod 700 ~/easy-rsa 
 
## 2. Creacion de una PKI para OpenVPN 
 
Para crear el directorio de PKI en el servidor de OpenVPN, completamos un archivo llamado vars con algunos valores predeterminados.
Primero, usamos cd para ingresar al directorio easy-rsa, despues creará y editará el archivo vars con el editor de texto que prefiera.
 
 Codigo: 

	cd ~/easy-rsa
	vi vars

 Una vez abierto el archivo, pegamos las siguientes dos líneas 
 
 Codigo: 
 
	set_var EASYRSA_ALGO "ec" 
	set_var EASYRSA_DIGEST "sha512" 
 
Al completar el archivo vars guardamos y cerramos, continuamos creando el directorio PKI., ejecutamos la secuencia de comandos easyrsa
con la opción init-pki. 
 
Codigo: 
 
	./easyrsa init-pki 
 
La PKI de nuestro servidor VPN solo se usa como una ubicación adecuada y centralizada para almacenar solicitudes de certificado y certificados públicos.
 
## 3. Creacion de la CA

Nos diríjimos al directorio ~/easy-rsa de nuestro servidor de OpenVPN. 
 
 Codigo: 
 
	cd ~/easy-rsa 
 
Invocamos easyrsa con la opción build-ca. 
 
Codigo: 
 
	./easyrsa build-ca

En este paso se nos pedira el nombre de nuestra CA y un password, los configuramos y listo, ya tendremos el certificado y la llave de nuestra CA.

## 4. Creacion de una solicitud de certificado de servidor de OpenVPN y una clave privada. 
 
Nos diríjimos al directorio ~/easy-rsa de nuestro servidor de OpenVPN. 
 
 Codigo: 
 
	cd ~/easy-rsa 
 
Invocamos easyrsa con la opción gen-req seguida del nombre  server. 
 
Codigo: 
 
	./easyrsa gen-req server 
 
Pide se le asigne una contrasena, despues asignamos el common Name
 
salida: 
 
	Common Name (eg: your user, host, or server name) [server]:server 

	Keypair and certificate request completed. Your files are: 
	req: /home/becarios/easy-rsa/pki/reqs/server.req 
	key: /home/becarios/easy-rsa/pki/private/server.key 

Se crean una clave privada para el servidor y un archivo de solicitud de certificado llamado server.req.

Copiamos el archivo server.key al directorio del servidor de OpenVPN.
 
Codigo: 

	sudo cp ./pki/private/server.key /etc/openvpn/server/ 

Hemos creado correctamente una clave privada para nuestro servidor de OpenVPN y generamos una solicitud de firma de certificado para el servidor de OpenVPN. 

## 5. Firmar la solicitud de certificado del servidor de OpenVPN 
 
Ahora el servidor de CA necesita conocer el certificado server y validarlo. Una vez que el CA valide y devuelva el certificado al servidor de OpenVPN, los 
clientes que confían en su CA podrán confiar también en el servidor de OpenVPN.

Firmamos la solicitud ejecutando la secuencia de comandos easyrsa con la opción sign-req seguida del tipo de solicitud y el nombre común.
El tipo de solicitud puede ser client o server. Como estamos trabajando con la solicitud de certificado del servidor de OpenVPN, nos aseguramos
de usar el tipo de solicitud server:
 
Codigo: 
 
	./easyrsa sign-req server server 
 
En seguida  solicitará que verificar que la solicitud provenga de una fuente de confianza. Escribimos yes y pulse ENTER para confirmar, en seguida solicitara 
la contrasena, la ingresamos y esperamos la firma. 
 
salida: 
 
	You are about to sign the following certificate. 
	Please check over the details shown below for accuracy. Note that this request 
	has not been cryptographically verified. Please be sure it came from a trusted 
	source or that you have verified the request checksum with the sender. 

	Request subject, to be signed as a server certificate for 3650 days: 

	subject= 
	commonName = server 

	Type the word 'yes' to continue, or any other input to abort. 
	Confirm request details: yes 
	
	Certificate created at: /home/becarios/easy-rsa/pki/issued/server.crt 


Copiamos los archivos server.crt y ca.crt al servidor de OpenVPN.
 
Codigo: 

	sudo cp ./issued/server.crt ca.crt /etc/openvpn/server/ 
 
## 6. Configurar materiales de cifrado de OpenVPN 
 
Para generar la clave tls-crypt antes compartida, ejecutamos lo siguiente en el servidor de OpenVPN en el directorio ~/easy-rsa.
 
 Codigo: 

	cd ~/easy-rsa 
	openvpn --genkey --secret ta.key 
 
Obtenemos un archivo llamado ta.key.Lo copiamos en el directorio /etc/openvpn/server/.
 Codigo: 
 
	sudo cp ta.key /etc/openvpn/server/ 
 
Con estos archivos agregados en el servidor de OpenVPN, estamos listos para crear certificados de cliente y archivos de clave para
 sus usuarios, que usaremos para conectarmnos con la VPN.

## 7. Configurar OpenVPN 
 
Primero, copiamos el archivo de muestra server.conf como punto de inicio para nuestro archivo de configuración:

 Codigo:

    sudo cp /usr/share/doc/openvpn/sample/sample-config-files/server.conf /etc/openvpn/server/

OpenVPN tiene muchas opciones de configuración disponibles para personalizar el servidor acorde nuestras necesidades, anexamos la configuración del servidor de OpenVPN en el archivo:

[Configuracion de server openVPN](server.conf)

## 8. Iniciar OpenVPN 
 
Configuramos OpenVPN para que inicie en el arranque y  pueda conectarse la VPN en cualquier momento siempre que el servidor se esté ejecutando.
 Codigo: 
 
	sudo systemctl -f enable openvpn-server@server.service 
 
iniciamos el servicio de OpenVPN
 
Codigo: 
 
	sudo systemctl start openvpn-server@server.service 
 
salida: 
 
	●openvpn-server@server.service - OpenVPN service for server 
	Loaded: loaded (/usr/lib/systemd/system/openvpn-server@.service; enabled; vendor preset: disabled) 
	Active: active (running) since Tue 2020-04-07 02:32:07 UTC; 1min 52s ago 
	Docs: man:openvpn(8) 
	https://community.openvpn.net/openvpn/wiki/Openvpn24ManPage 
	https://community.openvpn.net/openvpn/wiki/HOWTO 
	Main PID: 15868 (openvpn) 
	Status: "Initialization Sequence Completed" 
	Tasks: 1 (limit: 5059) 
	Memory: 1.2M 
	CGroup: /system.slice/system-openvpn\x2dserver.slice/openvpn-server@server.service 
	└─15868 /usr/sbin/openvpn --status /run/openvpn-server/status-server.log --status-v ersion 2 --suppress-timestamps --cipher AES-256-GCM --ncp-ciphers AES-256-GCM:AES-128-GCM:AES-256-CBC:AES-128-CBC:BF-CBC --config server.conf 
	. . . 
 
Terminamos la configuración de la parte del servidor para OpenVPN.

## 9. Generar certificados y llaves de los clientes 
 
Generaremos un par individual de llave y certificado de cliente, el primer par de certificado y llave se denominará client1

Comenzamos por crear una estructura de directorios dentro de nuestro directorio de inicio para almacenar los archivos de certificado y llave de cliente.
 Codigo: 
 
	mkdir -p ~/client-configs/keys 

Almacenaremos los pares de certificado y llave de clientes y los archivos de configuración en este directorio, bloqueamos permisos como medida de seguridad.

 Codigo: 
 
	chmod -R 700 ~/client-configs 
 
Vamos al directorio EasyRSA y ejecutamos la secuencia de comandos ​​​​​​easyrsa con las opciones gen-req y nopass (Solo para servidores,los clientes poseen  contrasena), junto con el nombre común para el cliente
 Codigo: 
 
	cd ~/easy-rsa 
	./easyrsa sign-req client client1 
 
Cuando  solicita, ingresamos yes para confirmar que  firmaremos la solicitud de certificado y que esta provino de una fuente confiable:
 
 Salida:
  
	Type the word 'yes' to continue, or any other input to abort. 
	Confirm request details: yes 

solicita la contraseña. 

Oprimimos ENTER para confirmar el nombre común.copiamos el archivo client1.key al directorio ~/client-configs/keys/, antes creado.
 
Codigo:
 
	sudo cp pki/private/client1.key ~/client-configs/keys/ 
 
Nos diríjimos al directorio de EasyRSA  y firmmamos la solicitud como lo hicimos para el servidor en los pasos anteriores.
 
 Codigo: 
 
	./easyrsa sign-req client client1 
 
Ingresamos yes para confirmar que deseamos firmar la solicitud de certificado y que esta provino de una fuente confiable.

 Salida: 
 
	Type the word 'yes' to continue, or any other input to abort. 
	Confirm request details: yes 
 
Solicitará la contraseña, la ingresamos.
Se creará un archivo de certificado de cliente llamado client1.crt. 

Copiamos los archivos client1.crt, ca.crt y ta.key al directorio ~/client-configs/keys/ también y establecemos los permisos correspondientes para el usuario sudo. 
 
 Codigo: 

	cp ~/easy-rsa/ta.key ~/client-configs/keys/ 
	cp ~/easy-rsa/pki/ca.crt ~/client-configs/keys/ 
	cp  pki/issued/ client1.crt  ~/client-configs/keys/ 
	sudo chown becarios:users ~/client-configs/keys/* 
 
Se generarán los certificados y las llaves de nuestro servidor y cliente, y se almacenarán en los directorios correspondientes de  nuestro servidor de OpenVPN.

## 10. Crear la infraestructura de configuración de clientes 
 
Crearemos los  archivos de configuración para clientes de OpenVPN, todos los clientes deben tener su propia configuración
 y alinearse con los ajustes mencionados en el archivo de configuración del servicio.
Creamos un nuevo directorio en el que almacenaremos los archivos de configuración de clientes dentro del directorio client-configs creado anteriormente
 
Codigo: 

	mkdir -p ~/client-configs/files 
 
Anexamos la configuración base del cliente de OpenVPN **client.conf**

[Configuracion de Cliente  openVPN](client.conf)

En nuestro caso generamos un script un tanto improvisado para que nos genere todos los archivos de varios clientes por medio de un archivo donde cada linea sera el nombre del cliente.

[Script generator_new_certificates](generator_new_certificates.sh) 

Este script nos ayudo para empaquetar los 5 archivos necesarios para cada cliente en un archivo .tar.gz, todos los archivos generados se guardaron en un carpeta distinta a las anteriores, `~/all_files_clients`

Tambien encontramos uno que nos devuelve un archivo .ovpn donde ya viene empaquetado todo en un solo archivo portable.
 
Compilamos la configuración de base con el certificado, la clave y los archivos de cifrado pertinentes, ubicamos la configuración generada
en el directorio ~/client-configs/files. Creamos un  nuevo archivo llamado make_config.sh en el directorio ~/client-configs:
 
Codigo: 
 
	vi ~/client-configs/make_config.sh 
 
Pegamos en su interior las siguientes lineas.
 
	#!/bin/bash 
 
	# First argument: Client identifier 
 
	KEY_DIR=~/client-configs/keys 
	OUTPUT_DIR=~/client-configs/files 
	BASE_CONFIG=~/client-configs/client.conf 
 
	cat ${BASE_CONFIG} \ 
	<(echo -e '<ca>') \ 
	${KEY_DIR}/ca.crt \ 
	<(echo -e '</ca>\n<cert>') \ 
	${KEY_DIR}/${1}.crt \ 
	<(echo -e '</cert>\n<key>') \ 
	${KEY_DIR}/${1}.key \ 
	<(echo -e '</key>\n<tls-crypt>') \ 
	${KEY_DIR}/ta.key \ 
	<(echo -e '</tls-crypt>') \ 
	> ${OUTPUT_DIR}/${1}.ovpn 

Guardamos y cerramos el archivo al termine.

Nos aseguramos de marcar este archivo como ejecutable. 
 
Codigo: 
 
	chmod 700 ~/client-configs/make_config.sh 

## 11. Generar las configuraciones de clientes 
 
Con la ayuda de un archivo que contenga los nombres de los clientes de esta forma:
`clientes`

	  becarios-15
	  cliente-2
	  client-3
  
Codigo: 
 
	./generator_new_certificates.sh clientes 
 
Dentro del script se nos pregunta si los necesitamos con password y sin el, ya solo debemos ingresar el password de la ca y del cliente si es el caso.
Se crearan los archivos en el directorio de `~/all_files_clients`
`/all_files_clients`	  

    becarios-15.tar.gz
	cliente-2.tar.gz
	client-3.tar.gz

Debe transferir este archivo al dispositivo que planee usar como cliente, pues estos archivos ya contienen los 5 archivos para configurar en cualquier maquina.
Y para la creacion de archivos .ovpn

Generaremos  un archivo de configuración para las credenciales del cliente nos dirigimos a directorio ~/client-configs y ejecutamos.
  
Codigo: 
 
	cd ~/client-configs 
	./make_config.sh client1 
 
Se creo un archivo llamado client1.ovpn  en el directorio ~/client-configs/files.
Debe transferir este archivo al dispositivo que planee usar como cliente.

## 12. Configuración del firewall (opcional)
Si se hace uso del firewall es recomendable hacer los siquientes pasos:
Vamos a especificar  algunas reglas de firewall.
instalamos firewall. 
Codigo: 
 
	yum install firewall 
 
Permitimos a OpenVPN en el firewall, indagamos la zona de firewalld activa. 
 
 Codigo: 
 
	sudo firewall-cmd --get-active-zones 
 
Si no encontramos la zona trusted que indique la interfaz tun0, agregamos el dispositivo VPN a esa zona: 
 
 Codigo: 
 
	sudo firewall-cmd --zone=trusted --add-interface=tun0 
	sudo firewall-cmd --permanent --zone=trusted --add-interface=tun0 
 
Agregamos el servicio openvpn a la lista de servicios permitidos por firewalld dentro de la zona activa y hacemos que el ajuste sea permanente.
 
 Codigo: 
 
	sudo firewall-cmd --permanent --add-service openvpn 
	sudo firewall-cmd --permanent --zone=trusted --add-service openvpn 
 
Aplicamos los cambios al firewall 
 
Codigo: 
 
	sudo firewall-cmd --reload 
 
Corroboramos que el servicio se ha integrado de manera satisfactoria. 
 
Codigo: 
 
	sudo firewall-cmd --list-services --zone=trusted 
 
salida: 
 
	openvpn 
 
Agregamos regla de enmascaramiento: 
 
 Codigo: 
 
	sudo firewall-cmd --add-masquerade 
	sudo firewall-cmd --add-masquerade --permanent 
 
Corroboramos que el enmascaramiento se haya agregado exitosamente 
 
 Codigo: 
 
	sudo firewall-cmd --query-masquerade
 
 Salida: 
 
	yes
 
Creamos la regla de enmascaramiento para la subred de OpenVPN 
 
 Codigo: 
 
	DEVICE=$(ip route | awk '/^default via/ {print $5}') 
	sudo firewall-cmd --permanent --direct --passthrough ipv4 -t nat -A POSTROUTING -s 10.8.0.0/24 -o $DEVICE -j MASQUERADE 
 
Cargamos nuevamente el firewalld para activar cambios 
 Codigo: 
 
	sudo firewall-cmd --reload

## 13. Instalar la configuración de cliente 
 
El dispositivo que deseamos conectar a la VPN debe contar con el archivo .tar.gz Para instalar openVPN debe seguir el manual anexado en el archivo:
 
[Manual de configuracion para conectarse con openvpn](manual_conexion_openvpn.md) 
 
El manual tambien contiene las instrucciones para efectuar la conexion a nuestra VPN.


Fuente de consulta:
https://www.digitalocean.com/community/tutorials/how-to-set-up-and-configure-an-openvpn-server-on-centos-8-es
