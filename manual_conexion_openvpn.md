 # Manual para conectarse con openvpn
 ## 1. Instalar openvpn
* Para sistemas Debían y sus derivados:

      sudo apt install openvpn
* Para sistemas RedHat y sus derivados:
Instalar los repositorios epel si no los tienen instalados.

      sudo yum install epel-release
* Una vez teniendo ya los repositorios epel ejecutamos:

      sudo yum update
      sudo yum install openvpn

## 2. Instalar los certificados y archivos de configuración
* Suponiendo que todos tienen su archivo example.tar.gz en su directorio hogar ~/ entonces debemos pasar el archivo example.tar.gz a la carpeta client de openvpn con el siguiente comando: * Puede que ocupes permisos de administrador.

      mv example.tar.gz /etc/openvpn/client/
* Enseguida nos movemos a dicha carpeta con:

      cd /etc/openvpn/client/
* Una vez dentro de la carpeta client ejecutamos el siguiente comando para descomprimir example.tar.gz

      tar -xvvpf example.tar.gz
* Verificamos que estén 5 archivos ademas de example.tar.gz, que le pertenezcan al usuario root y con los siguientes permisos.

      ls -l
        -rw------- 1 root root  1281 oct 15 21:31 ca.crt
        -rw-r--r-- 1 root root  3773 oct 15 21:31 client.conf
        -rw------- 1 root root  4642 oct 15 21:31 client.crt
        -rw------- 1 root root  1675 oct 15 21:53 client.key
        -rw-r--r-- 1 root root 20480 oct 15 21:53 example.tar.gz
        -rw------- 1 root root   636 oct 15 21:31 ta.key
* Borramos el archivo example.tar.gz con:

      rm -f example.tar.gz
* **Solo para equipos clientes y no para los equipos que son servidores.**
    * Si sé está configurando los archivos para iniciar openvpn desde tu laptop o una maquina que **No** pertenece a un servidor tendrás que ejecutar lo siguiente:

          mv client.key client.pem
          openssl rsa -in client.pem -out client.key
    * Te pedirá un password * Password que previamente se les dio *  una vez ingresado el password verificamos que se haya creado client.key

          ls -l
            -rw------- 1 root root 1281 oct 15 21:31 ca.crt
            -rw-r--r-- 1 root root 3773 oct 15 21:31 client.conf
            -rw------- 1 root root 4642 oct 15 21:31 client.crt
            -rw------- 1 root root 1675 oct 15 22:00 client.key
            -rw------- 1 root root 1675 oct 15 22:00 client.pem
            -rw------- 1 root root  636 oct 15 21:31 ta.key
    * Y borramos el archivo client.pem

          rm -f client.pem
* **Los siguientes pasos son para configurar los clientes asi como los equipos servidores.**
* Si lo estamos configurando desde un equipo con sistema RedHat o alguno derivado como CentOs, tendremos que modficar el archivo **client.conf** ya sea con **vi** o con **nano**, dentro del archivo buscamos **group nobody** cuando lo encontremos lo descomentamos eliminando **;** al inicio de la línea, una vez hecho esto agregamos **;** al inicio de la línea de **group nogroup** y guardamos.

      group nobody
      ;group nogroup
* Si sé está configurando desde una maquina con un sistema Debían o sus derivados no es necesario hacer lo anterior.
* Una vez hecho lo anterior solo si fue necesario, verificamos el estatus del servicio con:

      systemctl status openvpn-client@client.service
* Si el servicio está inactivo ejecutamos:

      systemctl start openvpn-client@client.service
* Si no está inactivo ejecutamos:

      systemctl restart openvpn-client@client.service
* Verificamos que en el estatus nos muestre activo y no tengamos errores.
## 3. Comprobación
* Comprobamos que se nos muestre el nombre de una interfaz de red como **tun0**

      ip addr
* Verificamos que tengamos respuesta del servidor openvpn

      ping 10.0.8.1

## 4. Solo para equipos con servidores
* En el caso de los servidores para que el servicio se inicie de forma automática con el sistema ejecutar lo siguiente:

      systemctl -f enable openvpn-client@client.service
