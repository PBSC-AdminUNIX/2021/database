#!/bin/bash
# Funcion general para mostrar errores
mensaje_error() {
    echo "Error: $error"
}
# Mensaje de exito al haber creado el usuario correctamente
mensaje_success() {
    echo -e "\tUsuario *** $user *** creado correctamente"
    echo -e "\tHome: $home"
    echo -e "\tShell: $shell"
    echo -e "\tUID: $uid"
    echo -e "\tGID: $gid"
    echo -e "\tGECOS: $gecos"
    echo -e "\tPASSWORD: $pass\n"
}
# Funcion que crea al usuario con los distintos parametros que se obtienen del archivo ingresado
create_user() {
    home=$(echo $line | cut -f6 -d:)
    shell=$(echo $line | cut -f7 -d:)
    uid=$(echo $line | cut -f3 -d:)
    gecos=$(echo $line | cut -f5 -d:)
    pass=$(echo $line | cut -f2 -d:)
    sudo useradd -m -d $home -s $shell -u $uid -g $gid -c $gecos -p $(openssl passwd -1 $pass) $user
    if [ $? -eq 0 ]; then
        mensaje_success
    else
        error="Line:$line_number: Hubo un error al crear el usuario $user"
        mensaje_error
    fi
}
# Se valida que el usuario no existe, si ya existe se muestra un error
exist_user() {
    user=$(echo $line | cut -f1 -d:)
    existe_user=$(id $user 2>/dev/null)
    if [ $? -eq 1 ]; then
        echo "El usuario $user no existe"
        create_user
    else
        error="Line:$line_number: El usuario $user ya existe"
        mensaje_error
    fi
}
# Se valida que el grupo exista sino muestra un error
exist_group() {
    gid=$(echo $line | cut -f4 -d:)
    if [ ! -z "${gid##*[!0-9]*}" ]; then
        exist_gid=$(cat /etc/group | cut -f3 -d: | egrep "^$gid$")
        if [ $? -eq 0 ]; then
            exist_user
        else
            error="Line:$line_number: No existe el grupo con el GID $gid"
            mensaje_error
        fi
    else
        error="Line:$line_number: El GID $gid no es valido"
        mensaje_error
    fi
}
# Se valida que la linea contenga los 7 parametros necesarios para crear el usuario
valid_line() {
    valid_format_line=$(echo $line | cut -f7 -d:)
    if [ -n "$valid_format_line" ]; then
        valid_format_line=1
    else
        valid_format_line=0
    fi
}
# Se lee linea por linea del archivo validando que su formato sea correcto para obtener de manera correcta los datos
read_archivo() {
    line_number=0
    while IFS= read -r line; do
        line_number=$(($line_number + 1))
        valid_line
        if [ $valid_format_line -eq 1 ]; then
            exist_group
        else
            error="Line:$line_number: El formato es incorrecto"
            mensaje_error
        fi
    done <"$file"
}
# Funcion principal, valida que el archivo sean validos y que existan
main() {
    if [ -n "$file" ]; then
        if [ -f "$file" ]; then
            read_archivo
        else
            error="No es un archivo o no existe $file"
            mensaje_error
        fi
    else
        error="Debe ingresar la ruta de un archivo"
        mensaje_error
    fi
}
# Obtenemos y guardamos en la variable file la ruta o el archivo ingresado
file=$1
# Ejecutamos la funcion principal
main
